import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
	return (
		<div className="ui container comments">
			<ApprovalCard>
				<CommentDetail 
					author="Sam" 
					timeAgo="Today at 6:00 PM" 
					content="This is some content."
					image={faker.image.avatar()}
				/>
			</ApprovalCard>
			<ApprovalCard>
				<CommentDetail 
					author="Alex" 
					timeAgo="Today at 2:00 PM" 
					content="This is some more content."
					image={faker.image.avatar()}
				/>
			</ApprovalCard>
			<ApprovalCard>
				<CommentDetail 
					author="Jane" 
					timeAgo="Yesterday at 3:00 PM" 
					content="I love lamp."
					image={faker.image.avatar()}
				/>
			</ApprovalCard>
			<ApprovalCard>
				<div>
					<h4>Warning</h4>
					<p>Are you sure you want to continue?</p>
				</div>
			</ApprovalCard>
		</div>
	);
}

ReactDOM.render(<App />, document.querySelector('#root'))